#include <iostream>

int cube(int num) {
    return num * num * num;
}

int main() {
    int input;

    std::cout << "Enter a number to get it's cube: ";
    std::cin >> input;

    std::cout << "The cube of "
	      << input << " is "
	      << cube(input)
	      << std::endl;
}
